'use strict';

module.exports = function(grunt) {
  grunt.config.set('jshint', {
    prod: {
      options: {
        jshintrc: 'js/dist/.jshintrc',
      },
      src: ['js/dist/**/*.js','!js/dist/**/*.min.js']
    },
    dev: {
      options: {
        jshintrc: 'js/src/.jshintrc',
      },
      src: ['js/*.js', 'js/src/**/*.js']
    },
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
};
