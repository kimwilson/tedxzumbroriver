'use strict';

module.exports = function(grunt) {
  grunt.config.set('copy', {
      foundation_js: {
        expand: true,
        cwd: 'bower_components/foundation/js/',
        src: '**',
        dest: 'js/libs/'
      }
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
};
