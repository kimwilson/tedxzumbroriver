'use strict';

module.exports = function(grunt) {
  grunt.config.set('cssmin', {
      dev: {
        files: [{
          expand: true,
          cwd: 'styles/',
          src: ['*.css', '!*.min.css'],
          dest: 'styles/',
          ext: '.min.css'
        }],
      },
      prod: {
        files: [{
          expand: true,
          cwd: 'styles/',
          src: ['*.css', '!*.min.css'],
          dest: 'styles/',
          ext: '.min.css'
        }],
      }
  });

  grunt.loadNpmTasks('grunt-contrib-cssmin');
};
