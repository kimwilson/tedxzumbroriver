'use strict';

module.exports = function (grunt) {

	// Project configuration.
	grunt.initConfig({
    	pkg: grunt.file.readJSON('package.json'),
  	});

  	// Load tasks and configuration.
	grunt.loadTasks('grunt');

	// Register alias tasks.
	grunt.registerTask('lint',
	    'Statically analyze the project JavaScript for errors and code style',
	    ['jshint']);

	grunt.registerTask('bowercopy',
	    'Copy Bower files',
	    ['copy']);

	grunt.registerTask('dev',
	    'Development environment.',
	    ['lint', 'watch']);

	grunt.registerTask('prod',
		'Production environment.',
		['lint:prod','sass:prod','concat','uglify:prod','cssmin:prod']);

	grunt.registerTask('default', ['dev']);

};