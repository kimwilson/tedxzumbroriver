<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package TEDxZumbroRiver
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
	    <div class="row">
	    <div class="small-12 medium-3 columns avatar" >
		    <?php if ( has_post_thumbnail() ) { ?>
	    		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        			<?php the_post_thumbnail(); ?>
    			</a>
			<?php } ?>
		</div>
		<div class="small-12 medium-9 columns" >
		<?php the_excerpt(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'tedxzumbroriver' ),
				'after'  => '</div>',
			) );
		?>
		</div>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php edit_post_link( esc_html__( 'Edit', 'tedxzumbroriver' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

