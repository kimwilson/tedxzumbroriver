<?php
/**
 * The template for displaying all single speakers.
 *
 * @package TEDxZumbroRiver
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="row">
			<div class="small-12 medium-9 columns">
				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="entry-header">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
							
						</header><!-- .entry-header -->
                        <div class="row">
                        	<div class="small-12 columns">
                        		<strong><?php the_field('title'); ?></strong>
                        	</div>  	
                        </div>
                        <div class="row" id="speaker-info">
                        	<div class="small-12 medium-5 large-4 columns avatar">
                        	    <?php if ( has_post_thumbnail() ) { the_post_thumbnail();  } ?><br>
								<?php the_field('location'); ?>
                        	    <div class="speaker-social">
                        	    <?php 
									if(get_field('facebook'))
									{
										echo '<a href="' . get_field('facebook') . '"><img src="' . get_template_directory_uri() . '/images/facebooklg.png' . '"  alt="facebook" class="social-icon"></a>';
									}

								?>
								<?php 
									if(get_field('twitter'))
									{
										echo '<a href="' . get_field('twitter') . '"><img src="' . get_template_directory_uri() . '/images/twitterlg.png' . '" alt="twitter" class="social-icon"></a>';
									}

								?>
								<?php 
									if(get_field('website'))
									{
										echo '<a href="' . get_field('website') . '"><img src="' . get_template_directory_uri() . '/images/web-red.png' . '" alt="website" class="social-icon"></a>';
									}

								?>
                        		</div>
							</div>
							<div class="small-12 medium-7 large-8 columns">
								<div class="entry-content">
									<?php the_content(); ?>
									
								</div><!-- .entry-content -->
							</div>
						</div>
						<div class="row">
                        	<div class="small-12 columns">
                        		<ul class="tabs" data-tab>
								  <li class="tab-title active"><a href="#panel1">Talk Details</a></li>
								  <li class="tab-title"><a href="#panel2">Video</a></li>
								</ul>
								<div class="tabs-content">
								  <div class="content active" id="panel1">
								    <strong><h3 style="margin-top: 10px;">
								    	<?php 
										    if(get_field('session_title'))
											{
												the_field('session_title');
											} ?>
								    </h3></strong>
								    <div class="session-desc">
								    	<?php 
										    if(get_field('session_description'))
											{
												the_field('session_description');
											} else { ?>
											<p><br>Sorry, we are only releasing the title of each talk...you'll have to use your imagination for now.  But we promise it will be great!</p>
											<?php } ?>
								    </div>
								  </div>
								  <div class="content" id="panel2">
								    <?php 
								    if(get_field('youtube_video'))
									{ ?>
										<div class="embed-container">
											<?php the_field('youtube_video'); ?>
										</div>
									<?php } else {  ?>
									    <p>The video of this talk will be posted following the event.</p>
									<?php } ?>
								  </div>
								</div>
                        	</div>
                        </div>
						<footer class="entry-footer">
							<?php tedxzumbroriver_entry_footer(); ?>
						</footer><!-- .entry-footer -->
					</article><!-- #post-## -->


				<?php endwhile; // End of the loop. ?>
			</div>

			<div class="small-12 medium-3 columns sidebar">
				<?php get_sidebar(); ?>
			</div>

		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
