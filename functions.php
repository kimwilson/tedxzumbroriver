<?php
/**
 * TEDxZumbroRiver functions and definitions
 *
 * @package TEDxZumbroRiver
 */

if ( ! function_exists( 'tedxzumbroriver_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function tedxzumbroriver_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on TEDxZumbroRiver, use a find and replace
	 * to change 'tedxzumbroriver' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'tedxzumbroriver', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'tedxzumbroriver' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'tedxzumbroriver_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // tedxzumbroriver_setup
add_action( 'after_setup_theme', 'tedxzumbroriver_setup' );


// Replaces the excerpt "more" text by a link
function new_excerpt_more($more) {
       global $post;
	return '...<a class="moretag" href="'. get_permalink($post->ID) . '"> Read more...</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tedxzumbroriver_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'tedxzumbroriver_content_width', 640 );
}
add_action( 'after_setup_theme', 'tedxzumbroriver_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function tedxzumbroriver_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Default Sidebar', 'tedxzumbroriver' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name' => 'Homepage Below Content',
		'id' => 'home-below-content-1',
		'description' => 'Appears on the homepage below page content',
		'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="row">',
		'after_widget' => '</div></aside>',
		'before_title' => '<div class="small-12 columns"><h2 class="widget-title">',
		'after_title' => '</h2></div>',
	) );
	register_sidebar( array(
		'name' => 'Speaker Application',
		'id' => 'speaker-application-1',
		'description' => 'Appears on the speaker application info page',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><hr>',
	) );
	register_sidebar( array(
		'name' => 'Speaker Application Form',
		'id' => 'speaker-application-2',
		'description' => 'Appears on the speaker application form page',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><hr>',
	) );
	register_sidebar( array(
		'name' => 'Become a Volunteer',
		'id' => 'become-a-volunteer-1',
		'description' => 'Appears on the volunteer info page',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><hr>',
	) );
	register_sidebar( array(
		'name' => 'Volunteer Application',
		'id' => 'volunteer-application-1',
		'description' => 'Appears on the volunteer application page',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><hr>',
	) );
	register_sidebar( array(
		'name' => 'Event page',
		'id' => 'event-page-1',
		'description' => 'Appears on the event info page',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><hr>',
	) );
	register_sidebar( array(
		'name' => 'Contact page',
		'id' => 'contact-1',
		'description' => 'Appears on the contact info page',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><hr>',
	) );
	register_sidebar( array(
		'name' => 'Become a Sponsor',
		'id' => 'become-a-sponsor-1',
		'description' => 'Appears on the sponsorship info page',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><hr>',
	) );
	register_sidebar( array(
		'name' => 'Sponsors Page',
		'id' => 'sponsors-1',
		'description' => 'Appears on the sponsor list page',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><hr>',
	) );
	register_sidebar( array(
		'name' => 'Pitch Night',
		'id' => 'pitch-night',
		'description' => 'Appears on the pitch night event page',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><hr>',
	) );
	register_sidebar( array(
		'name' => 'Footer Section 1',
		'id' => 'footer-sidebar-1',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><hr>',
	) );
	register_sidebar( array(
		'name' => 'Footer Section 2',
		'id' => 'footer-sidebar-2',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><hr>',
	) );
	register_sidebar( array(
		'name' => 'Footer Section 3',
		'id' => 'footer-sidebar-3',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3><hr>',
	) );
}
add_action( 'widgets_init', 'tedxzumbroriver_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function tedxzumbroriver_scripts() {
	wp_enqueue_style( 'tedxzumbroriver-style', get_stylesheet_uri() );

	/* Add Google Font */
	wp_enqueue_style('firstmomentsphoto-googlefont', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,400');

    /* Add Custom CSS - normalize, foundation, and custom */
	wp_enqueue_style( 'tedx-custom-style', get_stylesheet_directory_uri() . '/styles/custom.min.css', array(), '1' );

	/* Add Foundation JS */
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/js/libs/foundation.min.js', array( 'jquery' ), '1', true );
	wp_enqueue_script( 'foundation-modernizr-js', get_template_directory_uri() . '/js/libs/vendor/modernizr.js', array( 'jquery' ), '1', true );

 
	/* Foundation Init JS */
	wp_enqueue_script( 'tedx-custom-js', get_template_directory_uri() . '/js/dist/production.min.js', array(), '1', true );

	//wp_enqueue_script( 'tedxzumbroriver-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'tedxzumbroriver-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'tedxzumbroriver_scripts' );

/*
* Foundation 5 Top Bar Menu Walker Class for WordPress 3.9+
 */
 
class F5_TOP_BAR_WALKER extends Walker_Nav_Menu
{ 
	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) 
	{
        $element->has_children = !empty( $children_elements[$element->ID] );
        
        if(!empty($element->classes)){
        	$element->classes[] = ( $element->current || $element->current_item_ancestor ) ? 'active' : '';
        	$element->classes[] = ( $element->has_children ) ? 'has-dropdown' : '';	        
        }
		
        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }
    
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"sub-menu dropdown\">\n";
	}
    
	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
	{
		$item_output = '';
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		$output .= ( $depth == 0 ) ? '<li class="divider"></li>' : '';
		$class_names = $value = '';
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-'. $item->ID;
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        $class_names = ' class="' . esc_attr( $class_names ) . '"';
		$output.= $indent.'<li id="menu-item-'. $item->ID.'" '.$class_names.'>';
		
		if ( empty( $item->title ) && empty( $item->url )) 
		{
			$item->url = get_permalink($item->ID);
			$item->title = $item->post_title;
			
			$attributes = $this->attributes($item);
 
            $item_output .= '<a'. $attributes .'>';
			$item_output .= apply_filters( 'the_title', $item->title, $item->ID );
			$item_output .= '</a>';
		}
		else
		{
			$attributes = $this->attributes($item);
	
			$item_output = $args->before;
			$item_output .= '<a'. $attributes .'>';
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= '</a>';
			$item_output .= $args->after;
		}
		
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args, $id );
	}
	
	private function attributes($item)
	{
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		
		return $attributes;
	}
	
	public static function items_default_wrap($menu_text) {
		/**
		 * Set default menu for menus not yet linked to theme location
		 * Method courtesy of robertomatute - https://github.com/roots/roots/issues/939
		 */
		return str_replace('<ul>', '<ul class="right">', $menu_text);
	}
      
	public static function items_remove_defaut_wrapper() 
	{
		/**
		 * Remove default div wrapper around ul element
		 */
		?>
		<script type="text/javascript">
			jQuery(document).ready(function($){
				var default_nav = $(".top-bar-section > div > ul");
				if(default_nav.parent("div").hasClass("right") === true){
		  		default_nav.unwrap();
				}
			});
		</script>
		<?php
	}
}
 
add_filter('wp_page_menu', array('F5_TOP_BAR_WALKER','items_default_wrap'));
add_action('wp_head', array('F5_TOP_BAR_WALKER','items_remove_defaut_wrapper'));


/*
* Creating speaker CPT
*/

function tedx_custom_post_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Speakers', 'Post Type General Name', 'tedxzumbroriver' ),
		'singular_name'       => _x( 'Speaker', 'Post Type Singular Name', 'tedxzumbroriver' ),
		'menu_name'           => __( 'Speakers', 'tedxzumbroriver' ),
		'parent_item_colon'   => __( 'Parent Speaker', 'tedxzumbroriver' ),
		'all_items'           => __( 'All Speakers', 'tedxzumbroriver' ),
		'view_item'           => __( 'View Speaker', 'tedxzumbroriver' ),
		'add_new_item'        => __( 'Add New Speaker', 'tedxzumbroriver' ),
		'add_new'             => __( 'Add New', 'tedxzumbroriver' ),
		'edit_item'           => __( 'Edit Speaker', 'tedxzumbroriver' ),
		'update_item'         => __( 'Update Speaker', 'tedxzumbroriver' ),
		'search_items'        => __( 'Search Speakers', 'tedxzumbroriver' ),
		'not_found'           => __( 'Not Found', 'tedxzumbroriver' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'tedxzumbroriver' ),
	);
	
// Set other options for Custom Post Type
	
	$args = array(
		'label'               => __( 'speakers', 'tedxzumbroriver' ),
		'description'         => __( 'Conference Speakers', 'tedxzumbroriver' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		//'taxonomies'          => array( 'genres' ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite' => array( 'slug' => 'speakers' ),
	);
	
	// Registering your Custom Post Type
	register_post_type( 'speakers', $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/

add_action( 'init', 'tedx_custom_post_type', 0 );



/**
 * Hide email from Spam Bots using a shortcode.
 *
 * @param array  $atts    Shortcode attributes. Not used.
 * @param string $content The shortcode content. Should be an email address.
 *
 * @return string The obfuscated email address. 
 */
function wpcodex_hide_email_shortcode( $atts , $content = null ) {
	if ( ! is_email( $content ) ) {
		return;
	}

	return '<a href="mailto:' . antispambot( $content ) . '">' . antispambot( $content ) . '</a>';
}
add_shortcode( 'email', 'wpcodex_hide_email_shortcode' );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
