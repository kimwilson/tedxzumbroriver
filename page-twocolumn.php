<?php
/**
 * Template Name: Two Column Page
 *
 *
 * @package TEDxZumbroRiver
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="row">
			<div class="small-12 medium-6 columns">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

			<?php endwhile; // End of the loop. ?>
			</div>

			<div class="small-12 medium-6 columns sidebar">
				<?php 
					if(is_page('contact')) {
				
						if(is_active_sidebar('contact-1')){
							dynamic_sidebar('contact-1');
						}
			        } else if(is_page('event')) {
				
						if(is_active_sidebar('event-page-1')){
							dynamic_sidebar('event-page-1');
						}
			 
					} else if(is_page('pitch-night')) {
				
						if(is_active_sidebar('pitch-night')){
							dynamic_sidebar('pitch-night');
						}
			 
					} else {
						 get_sidebar(); 
					}
				?>
			</div>

		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
