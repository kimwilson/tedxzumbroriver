<?php
/**
 * Template Name: Page with Sidebar
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package TEDxZumbroRiver
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="row">
			<div class="small-12 medium-8 columns">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

			<?php endwhile; // End of the loop. ?>
			</div>

			<div class="small-12 medium-4 columns sidebar">
				<?php 
				if(is_page('speaker-application')) {
				
					if(is_active_sidebar('speaker-application-1')){
						dynamic_sidebar('speaker-application-1');
					}
			 
				} else if(is_page('speaker-application-form')) {
				
					if(is_active_sidebar('speaker-application-2')){
						dynamic_sidebar('speaker-application-2');
					}
			 
				} else if(is_page('volunteer')) {
				
					if(is_active_sidebar('become-a-volunteer-1')){
						dynamic_sidebar('become-a-volunteer-1');
					}
			 
				} else if(is_page('volunteer-application')) {
				
					if(is_active_sidebar('volunteer-application-1')){
						dynamic_sidebar('volunteer-application-1');
					}
			 
				} else if(is_page('event')) {
				
					if(is_active_sidebar('event-page-1')){
						dynamic_sidebar('event-page-1');
					}
			 
				} else if(is_page('sponsor')) {
				
					if(is_active_sidebar('become-a-sponsor-1')){
						dynamic_sidebar('become-a-sponsor-1');
					}
			 
				} else if(is_page('sponsors')) {
				
					if(is_active_sidebar('sponsors-1')){
						dynamic_sidebar('sponsors-1');
					}
			 
				} else if(is_page('contact')) {
				
					if(is_active_sidebar('contact-1')){
						dynamic_sidebar('contact-1');
					}
			 
				} else {
					 get_sidebar(); 
				}
				?>
			</div>

		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
