<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package TEDxZumbroRiver
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="off-canvas-wrap" data-offcanvas>
<div class="inner-wrap">
<!-- Off Canvas Menu -->
    <aside class="left-off-canvas-menu">
        <!-- whatever you want goes here -->
        <?php
					    wp_nav_menu(array(
					        'container' => false,
					        'menu_class' => 'none',
					        'theme_location' => 'primary'
					    ));
					?>
    </aside>

<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'tedxzumbroriver' ); ?></a>

	<header id="masthead" role="banner">
	    <div class="site-header">
	 	<div class="row">
	 		<div class="small-12 medium-6 large-4 columns">
			<div class="site-branding">
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo (get_stylesheet_directory_uri() . '/images/logo.png') ?>" alt="TEDxZumbroRiver"></a></h1>
			</div><!-- .site-branding -->
			</div>
			<div class="small-12 medium-6 large-4 columns">
			<div class="event-info">
				<div><strong>Thursday, May 5, 2016 at 1:00 pm</strong><br />Autumn Ridge Church, Rochester, MN</div>
			</div><!-- event info -->
			</div>
			<div class="small-12 show-for-large-up large-4 columns text-right">
			<div class="main-cta">
				<a href="/event/" class="button radius">Buy a Ticket!</a>
			</div><!-- call to action -->
			</div>
			
		</div>
		<div class="row">
			<div class="small-12 large-8 columns">
				
				<div class="contain-to-grid sticky stickynav" data-options="sticky_on: [medium,large]">
					<nav class="top-bar" data-topbar role="navigation" data-options="sticky_on: [medium,large]">
						
						<section class="top-bar-section">
							    <a href="left-off-canvas-menu" class="button show-for-small-only left-off-canvas-toggle menu-icon" ><span>MENU</span></a>
								<?php wp_nav_menu( array( 'container' => false, 'theme_location' => 'primary','menu_class'  => 'hide-for-small-only','walker' => new F5_TOP_BAR_WALKER()  )); ?>
							
						</section>
					</nav>
			    </div>
			</div>
			<div class="small-12 large-4 columns show-for-large-up text-right ">
				<a class="socialicon" href="http://www.instagram.com/tedxzumbroriver/"><img width="30px" src="<?php echo (get_stylesheet_directory_uri() . '/images/instagram.png') ?>" alt="Instagram"></a><a class="socialicon" href="http://www.facebook.com/tedxzumbroriver/"><img width="30px" src="<?php echo (get_stylesheet_directory_uri() . '/images/facebook.png') ?>" alt="Facebook"></a><a class="socialicon" href="http://www.twitter.com/tedxzumbroriver/"><img width="30px" src="<?php echo (get_stylesheet_directory_uri() . '/images/twitter.png') ?>" alt="Twitter"></a>
			</div>
		</div>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
