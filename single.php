<?php
/**
 * The template for displaying all single posts.
 *
 * @package TEDxZumbroRiver
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="row">
			<div class="small-12 medium-8 columns">
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'template-parts/content', 'single' ); ?>

					<?php //the_post_navigation(); ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template.
						//if ( comments_open() || get_comments_number() ) :
						//	comments_template();
						//endif;
					?>

				<?php endwhile; // End of the loop. ?>
			</div>

			<div class="small-12 medium-4 columns sidebar">
				<?php 
					 get_sidebar(); 
				?>
			</div>

		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
