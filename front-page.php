<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package TEDxZumbroRiver
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<main id="main" class="site-main" role="main">
		<div class="row">
			<div class="small-12  columns">
				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'template-parts/content', get_post_format() );
						?>

					<?php endwhile; ?>

					<?php the_posts_navigation(); ?>

				<?php else : ?>

					<?php get_template_part( 'template-parts/content', 'none' ); ?>

				<?php endif; ?>
			</div>
		</div>	
		
		<div id="latest-news">
			<div class="row">
			  	<div class="small-12 columns">
			  		<h2 class="latest-news"><a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">Latest News from <?php bloginfo('name'); ?></a></h2>
			  	</div>
			</div>
			<div class="row">
			<?php 
				$args = array(
		    		'posts_per_page' => 2,
		    		'order' => 'DESC'
				);

				$rp = new WP_Query( $args );

				if($rp->have_posts()) :
		    		while($rp->have_posts()) : $rp->the_post(); ?>
					<div class="small-12 medium-6 columns">
					<?php if ( has_post_thumbnail() ) { 
					    $src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium', false, '' );
					    ?>
					    <a href='<?php the_permalink(); ?>' >
					    <?php echo "<div class='newsitem' style='background-image:url( $src[0] )'></div>"; ?>
					    </a>
					<?php } ?>
					
					<?php the_title( sprintf( '<h3 class="news-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
					<?php 
		       		the_excerpt(); // displays the excerpt  ?>

					</div>
		    		<?php endwhile;
		    		wp_reset_postdata(); // always always remember to reset postdata when using a custom query, very important
				endif;
			?>
			</div>
		</div>

		<?php   if(is_active_sidebar('home-below-content-1')){ 
					dynamic_sidebar('home-below-content-1');   
				}
	    ?>

		
		</main><!-- #main -->
		
	</div><!-- #primary -->

<?php get_footer(); ?>
