<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package TEDxZumbroRiver
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="row">
			<div class="small-12 medium-4 columns">
				<?php
				if(is_active_sidebar('footer-sidebar-1')){
					dynamic_sidebar('footer-sidebar-1');
				}
				?>
			</div>
			<div class="small-12 medium-4 columns">
				<?php
				if(is_active_sidebar('footer-sidebar-2')){
					dynamic_sidebar('footer-sidebar-2');
				}
				?>
			</div>
			<div class="small-12 medium-4 columns">
				<?php
				if(is_active_sidebar('footer-sidebar-3')){
					dynamic_sidebar('footer-sidebar-3');
				}
				?>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->
<a class="exit-off-canvas"></a>
</div><!-- .inner-wrap -->
</div><!-- .off-canvas-wrap -->
<?php wp_footer(); ?>

</body>
</html>
